#include <iostream>
//#include "Piece.cpp"
#include "Player.h"
#include "Pawn.cpp"
using namespace std;
void initBoard(Piece *board[8][8],Piece *pieces[32]);
void print_board(Piece *board[8][8]);
int main()
{

    Piece *board[8][8]={NULL};

    Player player[2];
    player[0].setColour("white");
    player[1].setColour("black");
    

    Piece *pieces[32];
    
    Pawn pawn_white1("pawn","white",1,0);
    Pawn pawn_white2("pawn","white",1,1);
    Pawn pawn_white3("pawn","white",1,2);
    Pawn pawn_white4("pawn","white",1,3);
    Pawn pawn_white5("pawn","white",1,4);
    Pawn pawn_white6("pawn","white",1,5);
    Pawn pawn_white7("pawn","white",1,6);
    Pawn pawn_white8("pawn","white",1,7);


    Pawn pawn_black1("pawn","black",6,0);
    Pawn pawn_black2("pawn","black",6,1);
    Pawn pawn_black3("pawn","black",6,2);
    Pawn pawn_black4("pawn","black",6,3);
    Pawn pawn_black5("pawn","black",6,4);
    Pawn pawn_black6("pawn","black",6,5);
    Pawn pawn_black7("pawn","black",6,6);
    Pawn pawn_black8("pawn","black",6,7);
    
    pieces[0]=&pawn_white1;
    pieces[1]=&pawn_white2;
    pieces[2]=&pawn_white3;
    pieces[3]=&pawn_white4;
    pieces[4]=&pawn_white5;
    pieces[5]=&pawn_white6;
    pieces[6]=&pawn_white7;
    pieces[7]=&pawn_white8;
    
    pieces[8]=&pawn_black1;
    pieces[9]=&pawn_black2;
    pieces[10]=&pawn_black3;
    pieces[11]=&pawn_black4;
    pieces[12]=&pawn_black5;
    pieces[13]=&pawn_black6;
    pieces[14]=&pawn_black7;
    pieces[15]=&pawn_black8;

    initBoard(board,pieces);
    print_board(board);
    
    return 0;
}


void initBoard(Piece *board[8][8],Piece *pieces[32])
{
    int loc = 0;
    for(int i=0;i<2;i++)
    {
        for(int j=0;j<8;j++)
        {   
            board[i][j]=pieces[loc];
            loc++;
        }
    }
}

void print_board(Piece *board[8][8])
{
    cout<<"+--------+--------+--------+--------+--------+--------+--------+--------+"<<endl;
    for(int i=0;i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(board[i][j]!=NULL)
            {
                cout<<"|  "<<board[i][j]->getName()<<"  ";
            }
            else
                cout<<"|        ";
        }
        cout<<"|"<<endl;
        for(int j=0;j<8;j++)
        {
            if(board[i][j]!=NULL)
            {
                cout<<"|  "<<board[i][j]->getColour()<<" ";
            }
            else
                cout<<"|        ";
        }
        cout<<"|"<<endl;
        cout<<"+--------+--------+--------+--------+--------+--------+--------+--------+"<<endl;
    }
}
