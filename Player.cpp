#include"Player.h"


Player::Player()
{
    this->pieces_left=16;
}


Player::Player(string colour)
{
    this->colour = colour;
    this->pieces_left = 16;
}


string Player::getColour(void)
{
    return this->colour;
}


int Player::getPiecesCount(void)
{
    return this->pieces_left;
}


void Player::setPiecesCount(int count)
{
    this->pieces_left = count;
}


void Player::setColour(string colour)
{
    this->colour=colour;
}

