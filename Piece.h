#ifndef Piece_h
#define Piece_h
//#pragma once
#include <iostream>
#include <string>
using namespace std;
class Piece
{
    private:
        int row;
        int col;
        string name;
        string colour;
    public:
        Piece();
        Piece(string, string ,int, int);
        void setName(string);
        string getName(void);
        void setColour(string);
        string getColour(void);
        void setRow(int);
        int getRow(void);
        void setCol(int);
        int getCol(void);
        ~Piece();
};

#endif