#ifndef Pawn_h
#define Pawn_h
#pragma once
#include "Piece.h"
class Pawn : public Piece
{
    private:
        bool turn;
    public:
        Pawn();
        Pawn(string name,string colour , int x, int y);
        bool isValidMove(int, int,Piece *board[8][8]);
        ~Pawn();
};
#endif