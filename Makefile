CC=g++
CFLAGS= -Wall -Werror -std=c++11

all:main.o Piece.o Player.o King.o
	$(CC) $(CFLAGS) main.o Piece.o Player.o King.o -o main.out
main.o:main.cpp
	$(CC) $(CFLAGS) -c main.cpp -o main.o
Piece.o:Piece.cpp Piece.h
	$(CC) $(CFLAGS) -c Piece.cpp Piece.h
Player.o:Player.h Player.cpp
	$(CC) $(CFLAGS) -c Player.cpp Player.h
King.o:King.h King.cpp
	$(CC) $(CFLAGS) -c King.h King.cpp
clean:
	rm *.o *.out
