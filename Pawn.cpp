#include"Pawn.h"


Pawn::Pawn():Piece()
{
    turn = false;
}



Pawn::Pawn(string name, string colour,int x, int y):Piece(name,colour,x,y)
{
    
    turn = false;
}

bool Pawn::isValidMove(int x, int y,Piece *board[8][8])
{
    
    if(this->getColour() == "white" && this->getRow()>x && x>=0 && x<=7)
    {
        if(this->getCol()==y)
        {
            if(!this->turn)
            {
                if(this->getRow()-2 == x || this->getRow()-1 == x)
                {
                    return true;
                }
            }
        }
        else if((this->getCol()+1 == y || this->getCol()-1 == y)&& y>=0 && y<=7)
        {
            if(this->getRow()-1 == x)
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else if(this->getColour() == "black" && this->getRow() < x && x>=0 && x<=7)
    {
        if(this->getCol()==y)
        {
            if(!this->turn)
            {
                if(this->getRow()+2 == x || this->getRow()+1 == x)
                {
                    return true;
                }
            }
        }
        else if((this->getCol()+1 == y || this->getCol()-1 == y)&& y>=0 && y<=7)
        {
            if(this->getRow()+1 == x)
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
    return false;
}


Pawn::~Pawn()
{

}