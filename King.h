#ifndef King_h
#define King_h

#include "Piece.h"
class King:public Piece
{
    private:
    public:
        King();
        King(string,string,int,int);
        bool isValidMove(int, int, Piece *board[8][8]);

};
#endif