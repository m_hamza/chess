#ifndef Player_h
#define Player_h
#include<string>
#include<iostream>
using namespace std;
class Player
{
    private:
        string colour;
        int pieces_left;
    public:
        Player(string);
        Player();
        string getColour(void);   
        int getPiecesCount(void);
        void setPiecesCount(int);
        void setColour(string);
};
#endif