#include "King.h"


King::King():Piece()
{

}
King::King(string name, string colour, int x, int y):Piece(name,colour,x,y)
{

}

bool King::isValidMove(int x, int y, Piece *board[8][8])
{
    if(x>=0 && x<=7 && y>=0 && y<=7)
    {
        //Backward Move According To White and Forward according to Black
        if(this->getRow()+1 == x && this->getCol()==y)
        {
            return true;
        }
        /* Forward Move According To White and Backward According To Black */
        else if(this->getRow()-1 == x && this->getCol()==y)
        {
            return true;
        }
        /* Right Move According To White And Left Move According To Black */
        else if(this->getRow() == x && this->getCol()+1 == y)
        {
            return true;
        }
        /* Left Move According To White And Right Move According To Black */
        else if(this->getRow() == x && this->getCol()-1 == y)
        {
            return true;
        }
        /* Upper Left Diagonal Move According To White And Lower Right Diagonal Move According To Black*/
        else if(this->getRow()-1 == x && this->getCol()-1 == y)
        {
            return true;
        }
        /* Upper Right Diagonal Move According To White And Lower Left Diagonal Move According To Black */
        else if(this->getRow()-1 == x && this->getCol()+1 == y)
        {
            return true;
        }
        /* Lower Left Diagonal Move According To White And Upper Right Diagonal Move According To Black */
        else if(this->getRow()+1 == x && this->getCol()-1 == y)
        {
            return true;
        }
        /* Lower Right Diagonal Move According To White And Upper Left Diagonal Move According To Black */
        else if(this->getRow()+1 == x && this->getCol()+1 == y)
        {
            return true;
        }  
        else
        {
            return false;
        }
    }
    return false;
}