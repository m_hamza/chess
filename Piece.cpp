#include "Piece.h"

Piece::Piece()
{

}


Piece::Piece(string name, string colour, int row, int col)
{
    this->name = name;
    this->colour = colour;
    this->row = row;
    this->col = col;
}


void Piece::setName(string name)
{
   this->name = name;
}

void Piece::setColour(string colour)
{
    this->colour = colour;
}

void Piece::setRow(int row)
{
    this->row = row;
}

void Piece::setCol(int col)
{
    this->col = col;
}

string Piece::getName(void)
{
    return this->name;
}

string Piece::getColour(void)
{
    return this->colour;
}

int Piece::getRow(void)
{
    return this->row;
}

int Piece::getCol(void)
{
    return this->col;
}

Piece::~Piece()
{

}